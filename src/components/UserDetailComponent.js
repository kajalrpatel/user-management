import React from 'react';
import { Card,  CardBody, CardText, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap' ;
import { Link } from 'react-router-dom';
import {Loading} from './LoadingComponent';
import  UserForm from './userForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser,faEnvelope, faMapMarked,faPhone,faExternalLinkAlt,faBuilding } from "@fortawesome/free-solid-svg-icons";

    function RenderUser({user}){
            return(
                <div className='row'>
                        <div className='col-12 col-md-6 '>
                            <Card >
                                <CardBody>
                                    <CardTitle><FontAwesomeIcon icon={faUser} /> {user.name}</CardTitle>
                                    <CardText>
                                        <FontAwesomeIcon icon={faEnvelope} /> <a href={"mailto:"+user.email}>{user.email} </a>
                                    </CardText>
                                    <CardText>
                                        <FontAwesomeIcon icon={faMapMarked}  />
                                        <span className='ml-1'>{user.address.street}</span>,
                                        <span className='ml-1'>{user.address.suite}</span>,
                                        <span className='ml-1'>{user.address.city}</span> - <span>{user.address.zipcode}</span>
                                    </CardText>
                                    <CardText>
                                        <FontAwesomeIcon icon={faPhone} /> {user.phone}
                                    </CardText>
                                     <CardText>
                                        <a className='btn btn-primary' target='_blank' color='primary'  rel="noopener noreferrer"  href={'http://www.'+user.website}> Visit Website <FontAwesomeIcon icon={faExternalLinkAlt} />  </a>
                                    </CardText>
                                </CardBody>
                            </Card>
                        </div>
                        <div className='col-12 col-md-6'>
                            <Card >
                                <CardBody>
                                    <CardText>
                                        <FontAwesomeIcon icon={faBuilding} /> <strong>Company</strong>: {user.company.name}<br/>
                                            <span>{user.company.catchPhrase}</span>
                                    </CardText>
                                    {(user.company.bs && user.company.bs != null) ? <CardText>{user.company.bs}</CardText> : ''}
                                </CardBody>
                            </Card>
                        </div>
                </div>
            );
    }
   
    const UserDetail = (props) => {
        
        if(props.userLoading){
            return(
                <div className='container'>
                    <div className='row'>
                        <Loading />
                    </div>
                </div>
            )
        }
        else if(props.userErrMsg){
            return(
                <div className='container'>
                    <div className='row'>
                        <h4> {props.userErrMsg} </h4>
                    </div>
                </div>
            )
        }
        else if(props.user != null){
            const handleUpdate = (values) => {
                var id = props.user.id;
                props.updateUser(values,id); 
            }
            return(
                <div className="container">
                    <div className='row'>
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/Users'>Users</Link> </BreadcrumbItem>
                            <BreadcrumbItem active>{props.user.name}</BreadcrumbItem>
                        </Breadcrumb>
                    </div>
                    <div className='row'>
                         <div className='col-md-6'>
                            <h3>{props.user.name}</h3> 
                        </div>
                        <UserForm user = {props.user} type='update' handleUpdate={handleUpdate} />
                        <div className='col-md-12'>
                            <hr />
                        </div>
                    </div>
                    <RenderUser user = {props.user}  />
                </div>
            );
        }
        else{
            return(
                <div></div>
            )
        }

    }
export default UserDetail;