import React  from 'react';
import {Navbar,NavbarBrand} from 'reactstrap';


const Header = () =>{
    return(
            <React.Fragment>
                <Navbar dark color='primary'>
                <div className="container">
                    <NavbarBrand className='mr-auto'>
                        <h2>User Management</h2>
                    </NavbarBrand>
                </div>
                </Navbar>
         </React.Fragment>
    )
}

export default Header;