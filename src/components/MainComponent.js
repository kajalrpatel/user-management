import React, {Component } from 'react';

import { Switch, Route, withRouter,Redirect  } from 'react-router-dom';
import { connect } from 'react-redux';
import UserList from './UserComponent';
import UserDetail from './UserDetailComponent';
import { fetchUsers,postUser,removeUser,updateUser } from '../redux/ActionCreators';
import Header from './HeaderComponent';

const mapStateToProps = state => {
    return{
        users: state.users
    }
}

const mapDispatchtoProps = (dispatch) =>({
    fetchUsers: () => { dispatch(fetchUsers())},
    postUser: (user,id) => dispatch(postUser(user,id)),
    removeUser: (user) => dispatch(removeUser(user)),
    updateUser: (user,id) => dispatch(updateUser(user,id))
});

class Main extends Component {
    componentDidMount(){
        this.props.fetchUsers();
    }
    render() {
        const UserwithID = ({match}) => {
             return(
                 <UserDetail user={this.props.users.users.filter((users) => users.id === parseInt(match.params.userId,10))[0]} 
                 userLoading = {this.props.users.isLoading}
                 userErrMsg = {this.props.users.errmess}
                 updateUser = {this.props.updateUser}
                 />
             );
         }
    return (
        <div >
            <Header />
             <Switch>
                   <Route exact path ='/Users' component ={ () => <UserList users ={this.props.users.users} 
                                                                    postUser={this.props.postUser} removeUser={this.props.removeUser}/> } />
                   <Route path = '/Users/:userId' component = {UserwithID} />
                   <Redirect to='/Users' />
                </Switch>
        </div>
      );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchtoProps)(Main));