import React,{useState} from 'react';
import { Button, Modal, ModalBody,ModalHeader, Label } from 'reactstrap' ;
import { Control, Errors,LocalForm} from 'react-redux-form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt } from "@fortawesome/free-solid-svg-icons";

const required =(val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);
const validEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);
const validLat = (val) => /^[+-]?(([1-8]?[0-9])(\.[0-9]{1,6})?|90(\.0{1,6})?)$/i.test(val);
const validLng = (val) =>/^[+-]?((([1-9]?[0-9]|1[0-7][0-9])(\.[0-9]{1,6})?)|180(\.0{1,6})?)$/i.test(val);

const UserForm = (props) => {
    
    const [isModalOpen, toggleModel] = useState(false);
    return(
        <div className='col-md-6'>
            <div className='text-right'>
               <Button className='pull-right' onClick={()=>toggleModel(true)} color='primary'>
                <FontAwesomeIcon icon={faPencilAlt}/> 
                   {
                       props.type === 'update' ? ' Update User' : 'Add User'
                    } 
               </Button>
            </div>
            <Modal isOpen={isModalOpen} toggle={()=>toggleModel(!isModalOpen)} className='modal-lg'>
                <ModalHeader toggle={()=>toggleModel(!isModalOpen)} >Update User Information</ModalHeader>
                <ModalBody>
                    <LocalForm onSubmit={(values) => { (props.type === 'update' ?  props.handleUpdate(values) : props.handleSubmit(values)); toggleModel(false) } }>
                        <div className='form-group row'>
                            <div className ='col-md-4'>
                                <Label htmlFor= "name">Name </Label>
                                <Control.text  model='.name' id='name' name='name' defaultValue={props.user ? props.user.name : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(3), maxLength: maxLength(25)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".name"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 2 characters',
                                                    maxLength: 'Must be 25 characters or less'
                                                }}
                                />
                            </div>
                            <div className ='col-md-3'>
                                <Label htmlFor= "username">Username </Label>
                                <Control.text  model='.username' id='username' name='username' defaultValue={props.user ? props.user.username : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(3), maxLength: maxLength(15)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".username"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 2 characters',
                                                    maxLength: 'Must be 15 characters or less'
                                                }}
                                />
                            </div>
                            <div className ='col-md-5'>
                                <Label htmlFor= "email">Email </Label>
                                <Control.text  model='.email' id='email' name='email' defaultValue={props.user ? props.user.email : ''}
                                            className='form-control'
                                            validators={{
                                                required, validEmail
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".email"
                                                show="touched"
                                                messages={{
                                                    required: 'Required. ',
                                                    validEmail: 'Invalid Email Address'
                                                }}
                                />
                            </div>
                        </div>
                        <div className='form-group row'>
                            <div className ='col-md-6'>
                                <Label htmlFor= "street">Street </Label>
                                <Control.text  model='.address.street' id='street' name='street' defaultValue={props.user ? props.user.address.street : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(4), maxLength: maxLength(30)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".address.street"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 3 characters',
                                                    maxLength: 'Must be 30 characters or less'
                                                }}
                                />
                            </div>
                            <div className ='col-md-6'>
                                <Label htmlFor= "suite">Suite </Label>
                                <Control.text  model='.address.suite' id='suite' name='suite' defaultValue={props.user ? props.user.address.suite : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(2), maxLength: maxLength(20)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".address.suite"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 2 characters.',
                                                    maxLength: 'Must be 20 characters or less.'
                                                }}
                                />
                            </div>
                        </div>
                        <div className='form-group row'>
                            <div className ='col-md-6'>
                                <Label htmlFor= "city">City </Label>
                                <Control.text  model='.address.city' id='city' name='city' defaultValue={props.user ? props.user.address.city : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(2), maxLength: maxLength(20)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".address.city"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 2 characters.',
                                                    maxLength: 'Must be 20 characters or less.'
                                                }}
                                />
                            </div>
                            <div className ='col-md-6'>
                                <Label htmlFor= "zipcode">Zipcode </Label>
                                <Control.text  model='.address.zipcode' id='zipcode' name='zipcode' defaultValue={props.user ? props.user.address.zipcode : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(4), maxLength: maxLength(30)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".address.zipcode"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 3 characters',
                                                    maxLength: 'Must be 30 characters or less'
                                                }}
                                />
                            </div>
                        </div>
                        <div className='form-group row'>
                            <div className ='col-md-6'>
                                <Label htmlFor= "lat">Latitude </Label>
                                <Control.text  model='.address.geo.lat' id='lat' name='lat' defaultValue={props.user ? props.user.address.geo.lat : ''}
                                            className='form-control'
                                            validators={{
                                                required, validLat
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".address.geo.lat"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    validLat: 'Invalid Latitude.'
                                                }}
                                />
                            </div>
                            <div className ='col-md-6'>
                                <Label htmlFor= "lng">Longitude </Label>
                                <Control.text  model='.address.geo.lng' id='lng' name='lng' defaultValue={props.user ? props.user.address.geo.lng : ''}
                                            className='form-control'
                                            validators={{
                                                required, validLng
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".address.geo.lng"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    validLng: 'Invalid Longitude'
                                                }}
                                />
                            </div>
                        </div>
                        <div className='form-group row'>
                            <div className ='col-md-5'>
                                <Label htmlFor= "phone">Phone </Label>
                                <Control.text  model='.phone' id='phone' name='phone' defaultValue={props.user ? props.user.phone : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(2), maxLength: maxLength(30)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".phone"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 2 characters.',
                                                    maxLength: 'Must be 30 characters or less.'
                                                }}
                                />
                            </div>
                            <div className ='col-md-7'>
                                <Label htmlFor= "website">Website </Label>
                                <Control.text  model='.website' id='website' name='website' defaultValue={props.user ? props.user.website : ''}
                                            className='form-control'
                                            validators={{
                                                required, minLength: minLength(4), maxLength: maxLength(30)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".website"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 3 characters',
                                                    maxLength: 'Must be 30 characters or less'
                                                }}
                                />
                            </div>
                        </div>
                        <div className='form-group row'>
                            <Label htmlFor= "company" className='ml-3'><strong>Company </strong></Label>
                            <div className ='col-12'>
                               <Control.text  model='.company.name' id='c_name' name='c_name' defaultValue={props.user ? props.user.company.name : ''}
                                            className='form-control'
                                            placeholder='Company Name'
                                            validators={{
                                                required, minLength: minLength(4), maxLength: maxLength(30)
                                            }}
                                            />
                                <Errors
                                                className="text-danger"
                                                model=".company.name"
                                                show="touched"
                                                messages={{
                                                    required: 'Required.',
                                                    minLength: 'Must be greater than 3 characters',
                                                    maxLength: 'Must be 30 characters or less'
                                                }}
                                />
                            </div>
                            <div className ='col-md-5'>
                             <Control.textarea model=".company.catchPhrase" id='catchPhrase' name='catchPhrase' defaultValue={ props.user ? props.user.company.catchPhrase : ''}
                             cols='5' rows='3' className='form-control mt-1'
                               placeholder='Slogan'  />
                            </div>
                            <div className ='col-md-5'>
                             <Control.textarea model=".company.bs" id='bs' name='bs' cols='5' rows='3' defaultValue={props.user ? props.user.company.bs : ''} className='form-control mt-1'
                               placeholder='Business Standard'  />
                            </div>
                            
                        </div>
                        <Button type='submit'  value='submit' color='primary' className='mt-2'>Submit</Button>
                    </LocalForm>
                </ModalBody>
            </Modal>
        </div>
    );
}

export default UserForm;