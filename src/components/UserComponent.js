import React from 'react';
import { Button } from 'reactstrap' ;
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import UserForm from './userForm';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";

    const UserList = (props) => {
        const handleSubmit = (values) => {
            var id = props.users[props.users.length-1].id+1;
            props.postUser(values,id); 
        }
        const users_list = props.users.map((user)=>{
            return(
                    <tr key={user.id}>
                        <td>{user.id}</td>
                        <td>
                            <Link to={`/Users/${user.id}`}>{user.name}</Link>
                        </td>
                        <td>{user.email}</td>
                        <td>{user.phone}</td>
                        <td>
                            <span> {user.address.street}, </span>
                            <span> {user.address.suite}, </span>
                            <span> {user.address.city} - {user.address.zipcode}</span>
                        </td>
                        <td>{user.company.name}</td>
                        <td>
                            <Button color='danger' onClick={()=>props.removeUser(user)}><FontAwesomeIcon icon={faTrashAlt}/> </Button>
                        </td>
                    </tr>
            );
        });
        if(props.users.isLoading){
            return(
                <div className='container'>
                    <div className='row'>
                        <Loading />
                    </div>
                </div>
            )
        }
        else if(props.users.errmess){
            return(
                <div className='container'>
                    <div className='row'>
                        <h4> {props.errmess} </h4>
                    </div>
                </div>
            )
        }
        else{
            return(
                <div className="container">
                    <div className="row mt-3">
                        <div className='col-md-6'>
                             <h3>List of All Users</h3>
                        </div>
                        <UserForm type='add' handleSubmit={handleSubmit} oblength={props.users.length} />
                        <div className='col-md-12'>
                            <hr />
                        </div>
                    </div>
                    <div className="row">
                        <table className="table table-bordered">  
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Company Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {users_list}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }
    }
    
export default UserList;