import * as ActionTypes from './ActionTypes';

export const Users = (state = {
        users:[],
        isLoading : true,
        errmess: null
    }, action) => {
    switch(action.type){
        case ActionTypes.ADD_USERS:
            return {...state, users:action.payload, isLoading : false, errmess: null }
        case ActionTypes.USERS_LOADING:
            return {...state, users:[], isLoading : true, errmess: null}
        case ActionTypes.USERS_FAILED:
            return {...state, users:[], isLoading : false, errmess: action.payload }
        case ActionTypes.ADD_NEW_USER:
            var user = action.payload;
            return {...state, users: state.users.concat(user)};
        case ActionTypes.REMOVE_USER:
            return {...state, users: state.users.filter(user => action.payload !== user)};
        case ActionTypes.UPDATE_USER:
            //var userupdated = action.payload.user;
            return {...state, users: state.users.map((user) => 
                user.id === action.payload.id ? action.payload  : user
            ) };
        default: 
            return state;
    }
}