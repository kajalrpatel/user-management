import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl'

export const fetchUsers = () => (dispatch) =>{
    dispatch(usersLoading(true));
    return fetch(baseUrl + 'users')
    .then(response => {
        if (response.ok) {
            return response
        } else {
          var error = new Error('Error ' + response.status + ': ' + response.statusText);
          error.response = response;
          throw error;
        }
      },
      error => {
            var errmess = new Error(error.message);
            throw errmess;
      })
    .then(response =>  response.json())
    .then(users => dispatch(addUsers(users)))
    .catch(error => dispatch(usersFailed(error.message)));
}

export const usersLoading = () => ({
    type : ActionTypes.USERS_LOADING
});

export const usersFailed = (errmess) => ({
    type: ActionTypes.USERS_FAILED,
    payload: errmess
});

export const addUsers = (users) => ({
    type: ActionTypes.ADD_USERS,
    payload: users
});

export const addUser = (user) => ({
  type: ActionTypes.ADD_NEW_USER,
  payload: user
});

export const postUser = (user,id) => (dispatch) => {
  var newuser = {
    ...user,
    id: id
  }
  dispatch(addUser(newuser))
};

export const removeUser = (user) => ({
  type: ActionTypes.REMOVE_USER,
  payload: user
});

export const updateUserInfo = (user) => ({
  type: ActionTypes.UPDATE_USER,
  payload: user
});

export const updateUser = (user,id) => (dispatch) => {
  var updatedUser = {
    id: id,
    ...user
  }
  dispatch(updateUserInfo(updatedUser))
}; 
