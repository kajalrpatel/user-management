export const USERS_LOADING = 'USERS_LOADING';
export const USERS_FAILED = 'USERS_FAILED';
export const ADD_USERS = 'ADD_USERS';
export const ADD_NEW_USER = 'ADD_NEW_USER';
export const REMOVE_USER = 'REMOVE_USER';
export const UPDATE_USER = 'UPDATE_USER';