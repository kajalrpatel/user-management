
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## User Management Web Application (React/Redux)

Below are the features:

1.	Page 1 : list of users as table layout, Retrieved data from API (Save data in Redux store)
2.	Page 1 : User Can add New user by using button `Add User`
3.	Page 1 : user can be deleted from list by clicking trash icon
4.	Page 1 : User will navigate to a detail page to display selected user ny clicking on User's Name
5.	Page 2: User can modify/update the user’s information with button `Update User`. 
6.	When user navigate back to Page 1, user can see the change.


In the project directory, you can run:

### Steps to Run this Project

1. clone this project using `git clone https://kajalrpatel@bitbucket.org/kajalrpatel/user-management.git`
2. Navigate to your folder `cd user-management`
3. install all dependncies `npm install`
4. Run the application on localhost `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.



## Live Demo

https://stackblitz.com/edit/react-6q7ifo

To learn React, check out the [React documentation](https://reactjs.org/).